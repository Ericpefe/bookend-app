import React, {useEffect, useState} from 'react';
import { StyleSheet, Text, View, Button, TextInput, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';


export default function Read({ navigation }) {

  
  const [text, setText] = useState("")
  const [textReview, setTextReview] = useState("")
  const [read, setRead] = useState([])
  const [nameClass, setNameClass] = useState(styles.textHidden)
  const [nameReview, setNameReview] = useState("Show reviews")


  const _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('DB');
      if (value !== null) {
        let valueParsed = JSON.parse(value)
        setRead(valueParsed)
      }
    } catch (error) {
    }
  };

  useEffect(() => {
     _retrieveData()
  }, [])

 

  const _storeData = async () => {
    try {
      await AsyncStorage.setItem(
        'DB',
        JSON.stringify(read)
      );
      setText("")
      setTextReview("")
      setRead([])
      _retrieveData()
    } catch (error) {
    }
  };


  const _retrieveFavorite = async (book) => {
    try {
      const value = await AsyncStorage.getItem('Favorites');
      if (value !== null) {
        let valueParsed = JSON.parse(value)
        valueParsed.push(book)
        _storeFavorite(valueParsed)
      }else {
        const firstArr = [book]
        _storeFavorite(firstArr)
      }
    } catch (error) {
    }
  };


  const _storeFavorite = async (book) => {
    try {
      const temp = book
      await AsyncStorage.setItem(
        'Favorites',
        JSON.stringify(temp)
      );
      
    } catch (error) {
    }
  };
  
  

    const deleteBook = (idx) => {
      let temp = read
      temp.splice(idx, 1)
      setRead(temp)
      _storeData()
    }





  const displayBook = (book, idx) => {
  console.log(read)
return (
  <View key={idx} style={styles.box}>
    <View style={styles.box}>
    <Text style={styles.text}>{book.name}</Text>
    <Text style={nameClass}>{book.review}</Text>
    </View>
    <View style={styles.boxButton}>
    <Button onPress={()=>_retrieveFavorite(book)} color={'#667292'} title='Add to favorite'/>
    <Button onPress={()=>deleteBook(idx)} color={'#667292'} title='X'/>
    </View>
  </View>
)
    
  }


  const addRead = () => {
    if (text !== ""){
    let book = {
      name: text,
      review: textReview
    }
    let temp = read
    temp.push(book)
    setRead(temp)
    _storeData()
  }else {
    alert("Type the name of the book")
  }
  }


  const changeName = () => {
    if (nameClass === styles.textHidden){
      setNameClass(styles.textReview)
      setNameReview("Hidde reviews")
    }else {
      setNameClass(styles.textHidden)
      setNameReview("Show reviews")
    }
  }

  
  return (
     
    <View style={styles.container}>
      <ScrollView>
      <View style={styles.boxButton}>
         <Button
  color={'#c94c4c'}
  title="Home"
  onPress={() => navigation.navigate('Home')}
/>
        <Button
  color={'#c94c4c'}
  title="Favorites"
  onPress={() => navigation.navigate('Favorites')}
/>
</View>
      <View style={styles.inputBox}>
       <TextInput
                    style={styles.input}
                    onChangeText={(text) => setText(text)}
                    placeholder='Name of the book'
                    value={text}
                />
                 <TextInput
                    style={styles.inputReview}
                    onChangeText={(text) => setTextReview(text)}
                    placeholder='Type a review if you desire...'
                    value={textReview}
                />
      <Button onPress={addRead} color={'#667292'} title="Add"/>
      </View>
      {read.map((book, idx)=>{return displayBook(book, idx)})}
      <View style={styles.boxButton}>
    <Button color={'#667292'} onPress={changeName} title={nameReview}/>
    </View>
    </ScrollView>
    </View>
    
  )
  }

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#e6e2d3',
    alignItems: 'center',
    height: '100%'
  },
  input:{
    height: 40, 
    borderColor: 'gray', 
    width: 180,
    borderWidth: 1,  
  },
  inputReview:{
    height: 150, 
    borderColor: 'gray', 
    width: 180,
    borderWidth: 1  
  },
  text:{
    fontSize:20,
    color:'black',
    padding: 0.5,
    borderWidth: 0.5,
    borderColor: 'black',
    fontFamily: 'Courier New',
  },
  textReview:{
    fontSize:20,
    color:'black',
    maxWidth: '90%',
    fontFamily: 'Courier New'
  },
  textHidden:{
    display: 'none',
  },
  box:{
    marginTop: 20,
    width:'80%',
    justifyContent:'center',
    alignItems:'center'
  },
  boxButton:{
    flexDirection:'row',
    width:'80%',
    justifyContent:'center',
    alignItems:'center'
  },
  inputBox:{
    marginTop: 25,
    alignItems:'center',
    justifyContent:'center',
  }
});