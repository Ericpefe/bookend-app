import React, { useState, useEffect} from 'react';
import { StyleSheet, Text, View, Button, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function Favorites({ navigation }){

  const [read, setRead] = useState([])
  const [favorites, setFavorites] = useState([])

  const _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('DB');
      if (value !== null) {
        let valueParsed = JSON.parse(value)
        setRead(valueParsed)
      }
    } catch (error) {
    }
  };

  const _retrieveFavorite = async () => {
    try {
      const value = await AsyncStorage.getItem('Favorites');
      if (value !== null) {
        let valueParsed = JSON.parse(value)
        setFavorites(valueParsed)        
      }
    } catch (error) {
    }
  };

  useEffect(() => {
     _retrieveData()
     _retrieveFavorite()
  }, [])

  const pluralRead = () => {
    if (read.length === 1){
      return 'book'
    }else {
      return 'books'
    }
  }

  const pluralFavourites = () => {
    if (favorites.length === 1){
      return 'book'
    }else {
      return 'books'
    }
  }

return (
    <View style={styles.container}>
      <ScrollView>
    <View style={styles.box}>
      <Text
      numberOfLines={1} 
      style={styles.text}>
        BookEnd
        </Text>
    </View>
    <View style={styles.box}>
    <Text
        style={styles.textExplain}>
        This app allows you to keep track of everything you read, write a review and add to your favorites.
        </Text>
      </View> 
      <View style={styles.boxButton}>
      <Button
  color={'#c94c4c'}
  title="Update"
  onPress={() => _retrieveData, _retrieveFavorite}
  />
  </View>
      <View style={styles.boxButton}>
        <Text style={styles.textButton}>
          {`You've read ${read.length} ${pluralRead()}.`}
        </Text>
        <Button
  color={'#c94c4c'}
  title="Read"
  onPress={() => navigation.navigate('Read')}
/>
</View>
<View style={styles.boxButton}>
        <Text style={styles.textButton}>
          {`You've added ${favorites.length} ${pluralFavourites()} to your favorites.`}
        </Text>    
        <Button
  color={'#c94c4c'}
  title="Favorites"
  onPress={() => navigation.navigate('Favorites')}
/>
</View> 
</ScrollView>
  </View>
)
    }


const styles = StyleSheet.create({
    container: {
      top:30,
      backgroundColor: '#e6e2d3',
      alignItems: 'center',
      height: '100%'
    },
    text:{
      fontSize:50,
      color:'black',
      fontFamily: 'Courier New'
    },
    textExplain:{
      fontSize:20,
      color:'black',
      width: '75%',
      fontFamily: 'Courier New'
    },
    textButton:{
      fontSize:20,
      color:'black',
      width: '55%',
      fontFamily: 'Courier New'
    },
    box:{
      flexDirection:'row',
      width:'80%',
      justifyContent:'center',
      alignItems:'center',
      marginTop: 25
    },
    boxButton:{
      marginTop: 70,
      width:'80%',
      justifyContent:'center',
      alignItems:'center'
    },
  });