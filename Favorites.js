import React, {useEffect, useState} from 'react';
import { StyleSheet, Text, View, Button, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';




export default function Favorites({ navigation }) {

  const [books, setBooks] = useState([])
  const [nameClass, setNameClass] = useState(styles.textHidden)
  const [nameReview, setNameReview] = useState("Show reviews")

  const _retrieveFavorite = async () => {
    try {
      const value = await AsyncStorage.getItem('Favorites');
      if (value !== null) {
        let temp = books
        let valueParsed = JSON.parse(value)
        setBooks(valueParsed)
      }
    } catch (error) {
    }
  };

  useEffect(()=>{
    _retrieveFavorite()
  }, [])
  

  const _storeFavorite = async (temp) => {
    try {
      await AsyncStorage.setItem(
        'Favorites',
        JSON.stringify(temp)
      );
    } catch (error) {
    }
  };

  const deleteBook = (idx) => {
    let temp = books
    temp.splice(idx, 1)
    _storeFavorite(temp)
    _retrieveFavorite()
  }


  const changeName = () => {
    if (nameClass === styles.textHidden){
      setNameClass(styles.textReview)
      setNameReview('Hidde reviews')
    }else {
      setNameClass(styles.textHidden)
      setNameReview('Show reviews')
    }
  }

  const renderBook = (book, idx) => {
    return(
      <View key={idx} style={styles.box}>
      <View style={styles.box}>
      <Text style={styles.text}>{book.name}</Text>
      <Text style={nameClass}>{book.review}</Text>
      </View>
      <View style={styles.box}>
      <Button onPress={()=>deleteBook(idx)} color={'#667292'} title='X'/>
      </View>
    </View>
    )
  }

  return(
    
    <View style={styles.container}>
    <ScrollView>
    <View style={styles.boxButton}>
         <Button
  color={'#c94c4c'}
  title="Home"
  onPress={() => navigation.navigate('Home')}
/>
        <Button
  color={'#c94c4c'}
  title="Read"
  onPress={() => navigation.navigate('Read')}
/>
</View>
      {books.map((book,idx)=>renderBook(book, idx))}
      <View style={styles.boxButton}>
    <Button color={'#667292'} onPress={changeName} title={nameReview}/>
    </View>
    </ScrollView>
    </View>
    
  )

}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#e6e2d3',
    alignItems: 'center',
    height: '100%'
  },
  text:{
    fontSize:20,
    color:'black',
    padding: 0.5,
    borderColor: 'black',
    width: '70%',
    borderWidth: 0.5,
    fontFamily: 'Courier New'
  },
  textReview:{
    fontSize:20,
    color:'black',
    width: '50%',
    fontFamily: 'Courier New'
  },
  textHidden:{
    fontSize:20,
    display: 'none',
    color:'black',
    width: '75%',
    fontFamily: 'Courier New'
  },
  box:{
    width:'80%',
    marginLeft: 20,
    marginTop: 25,
  },
  boxButton:{
    marginTop: 20,
    flexDirection:'row',
    width:'80%',
    marginLeft: 120,
  },
});